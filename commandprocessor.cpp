/***************************************************************************
 *   Copyright 2018 by Brandon Hall                                        *
 *   brandonjhall@windstream.net                                           *
 *                                                                         *
 *   This file is part of AUR Browser                                      *
 *   AUR Browser is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   AUR Browser is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with AUR Browser. If not, see <http://www.gnu.org/licenses/>.   *
 **************************************************************************/

#include <QApplication>
#include <QProcess>
#include <QDebug>
#include <QDir>

#include "commandprocessor.h"

CommandProcessor::CommandProcessor(QObject *parent) : QObject(parent)
{
}

void CommandProcessor::makepkg(const QString &package, const QString &archive)
{
    if(isRunning) {
        QPair<QString,QString> item;

        item.first = package;
        item.second = archive;
        que.append(item);
        return;
    }

    currentPackage = package;
    nextFunction = qstrdup("buildPackage");
    extractTar(archive);
    isRunning = true;
}

void CommandProcessor::buildPackage()
{
    currentProcess = setupProcess();
    nextFunction = qstrdup("installPackage");

    currentProcess->setProgram("makepkg");
    currentProcess->start();
}

void CommandProcessor::installPackage()
{
    currentProcess = setupProcess();
    delete[] nextFunction;
    nextFunction = qstrdup("");
    currentProcess->setProgram("bash");
    currentProcess->setArguments(QStringList() << "-c" << "sudo -A -v && makepkg -sic");
    currentProcess->start();
}

void CommandProcessor::extractTar(QString file)
{
    QString dir = QString(file).remove(".tar.gz");
    currentProcess = setupProcess();

    currentProcess->setProgram("tar");
    currentProcess->setArguments(QStringList() << "-xzf" << file);
    currentProcess->start();
    packageDir = dir;
}

QProcess *CommandProcessor::setupProcess()
{
    QProcess *proc = new QProcess(this);

    proc->setProcessChannelMode(QProcess::MergedChannels);

    connect(proc, &QProcess::readyReadStandardOutput, this, [proc, this]() {
        proc->setReadChannel(QProcess::StandardOutput);
        processOutput();
    });

    connect(proc, &QProcess::readyReadStandardError, this, [proc, this]() {
        proc->setReadChannel(QProcess::StandardError);
        processOutput();
    });

    connect(proc, &QProcess::errorOccurred, this, [this](QProcess::ProcessError error) {
        Q_UNUSED(error)
    });

    connect(proc, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this,
            [proc, this](int, QProcess::ExitStatus exitStatus) {
        if(exitStatus == QProcess::NormalExit) {
            if(!QString(nextFunction).isEmpty())
                QMetaObject::invokeMethod(this, nextFunction, Qt::QueuedConnection);

            if(proc->program() == "tar")
                QDir::setCurrent(packageDir);

            if(proc->program() == "bash") {
                emit finished(currentPackage);
                isRunning = false;
                queNext();
            }
        } else {
            isRunning = false;
            delete[] nextFunction;
            nextFunction = qstrdup("");
            queNext();
        }
    });

    return proc;
}

void CommandProcessor::processOutput()
{
    while(currentProcess->canReadLine()) {
        QByteArray out = currentProcess->readLine();
        m_output.append(out);
        emit outputReady();
        QApplication::processEvents();
    }

    if(!currentProcess->atEnd()) {
        QString remain = currentProcess->readAll();
        QChar defaultChoice;

        m_output.append(remain);
        if(remain.contains("[y/n]", Qt::CaseInsensitive)) {
            if(remain.contains("[Y/"))
                defaultChoice = 'Y';
            else
                defaultChoice = 'N';

            emit outputReady();
            emit ynPrompt(remain, defaultChoice);
        }
    }
}

QString CommandProcessor::output() const
{
    return m_output;
}

void CommandProcessor::decline()
{
    currentProcess->write("n\n");
}

void CommandProcessor::accept()
{
    currentProcess->write("y\n");
}

void CommandProcessor::queNext()
{
    if(que.count() > 0) {
        QPair<QString,QString> item = que.takeFirst();

        nextFunction = qstrdup("buildPackage");
        currentPackage = item.first;
        extractTar(item.second);
        isRunning = true;
    }
}
