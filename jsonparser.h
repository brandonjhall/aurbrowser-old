/***************************************************************************
 *   Copyright 2018 by Brandon Hall                                        *
 *   brandonjhall@windstream.net                                           *
 *                                                                         *
 *   This file is part of AUR Browser                                      *
 *   AUR Browser is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   AUR Browser is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with AUR Browser. If not, see <http://www.gnu.org/licenses/>.   *
 **************************************************************************/

#ifndef JSONPARSER_H
#define JSONPARSER_H
#define PKG_PATH 0
#define PKG_NAME 1
#define PKG_VER 2
#define PKG_WEB 3
#define PKG_VOTE 4
#define PKG_POP 5
#define PKG_MAIN 6
#define PKG_DESC 7
#define PKG_BASE 8

#include <QObject>

class JsonParserPrivate;
class QStandardItemModel;

class JsonParser : public QObject
{
    Q_OBJECT
public:
    explicit JsonParser(QObject *parent = nullptr);
    JsonParser(const QByteArray data, QObject *parent = nullptr);

    void setData(const QByteArray data);
    QStandardItemModel *model() const;
    QString errorString() const;
    int numResults() const;

    static QStringList getSearchColumns();

    static QStringList getMultiinfoColumns();

    QList<QStringList> getDetails() const;

signals:
    void error();
    void modelReady();
    void detailsReady();

public slots:
    void processData();

private:
    void processMultiInfo(QJsonObject *obj);
    void processSearch(QJsonObject *obj);
    void processError(QJsonObject *obj);

    QList<QStringList> m_details;
    QStandardItemModel *m_model;
    JsonParserPrivate *priv;
    QString m_errorString;
    int m_numResults = 0;

    static const QStringList searchColumns;
    static const QStringList multiinfoColumns;
};

#endif // JSONPARSER_H
