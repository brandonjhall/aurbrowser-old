#include "networkengine.h"

#include <QNetworkReply>
#include <QApplication>
#include <QMessageBox>
#include <QFile>

NetworkEngine::NetworkEngine(QObject *parent) : QNetworkAccessManager(parent)
{
    connectToHostEncrypted("aur.archlinux.org");
    connectToHostEncrypted("aur.archlinux.org");
}

void NetworkEngine::findPackage(const QString field, const QString searchText)
{
    QString url = QString("https://aur.archlinux.org/rpc/?v=5&type=search&by=%1&arg=%2").arg(field, searchText);
    sendRPCRequest(QUrl(url));
}

void NetworkEngine::getPackageDetails(const QString pkgName)
{
    QString url = QString("https://aur.archlinux.org/rpc/?v=5&type=info&arg[]=%1").arg(pkgName);
    sendRPCRequest(QUrl(url));
}

void NetworkEngine::downloadFileToMemory(QUrl url)
{
    QNetworkRequest request(url);
    QNetworkReply *reply = get(request);

    connect(reply, &QNetworkReply::finished, this, [reply, this]() {
        if(reply->error()) {
            error(reply);
            return;
        }

        m_fileData = reply->readAll();
        emit fileDataReady();
        reply->deleteLater();
    });
}

void NetworkEngine::downloadFile(QUrl url)
{
    QNetworkRequest request(url);
    QNetworkReply *reply = get(request);

    connect(reply, &QNetworkReply::finished, this, [reply, url, this]() {
        if(reply->error()) {
            error(reply);
            return;
        }

        QString fileName = m_tempPath + "/" + url.fileName();
        QFile file(fileName);

        file.open(QFile::ReadWrite);

        if(file.isOpen()) {
            file.write(reply->readAll());
            file.close();
            emit fileSaved(fileName);
        }

        reply->deleteLater();
    });
}

void NetworkEngine::sendRPCRequest(QUrl url)
{
    QNetworkRequest req(url);
    QNetworkReply *reply = get(req);

    connect(reply, &QNetworkReply::finished, [reply, this]() {
        if(reply->error()) {
            error(reply);
            return;
        }

        m_data = reply->readAll();

        if(!m_data.isEmpty())
            emit dataReady();

        reply->deleteLater();
    });
}

void NetworkEngine::error(QNetworkReply *reply)
{
    QMessageBox mbox(QApplication::activeWindow());

    mbox.setText(reply->errorString());
    mbox.setWindowTitle("Network Error");
    mbox.setIcon(QMessageBox::Warning);
    mbox.exec();
    reply->deleteLater();
}

QString NetworkEngine::getFileData() const
{
    return m_fileData;
}

void NetworkEngine::setTempPath(const QString &tempPath)
{
    m_tempPath = tempPath;
}

QByteArray NetworkEngine::getData()
{
    QByteArray arr = m_data;
    m_data.clear();
    return arr;
}
