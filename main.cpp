/***************************************************************************
 *   Copyright 2018 by Brandon Hall                                        *
 *   brandonjhall@windstream.net                                           *
 *                                                                         *
 *   This file is part of AUR Browser                                      *
 *   AUR Browser is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   AUR Browser is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with AUR Browser. If not, see <http://www.gnu.org/licenses/>.   *
 **************************************************************************/

#include "mainwindow.h"

#include <QApplication>
#include <QInputDialog>
#include <QTextStream>
#include <QDir>

#include <QNetworkAccessManager>

static int runMainWindow(QApplication *app);
static int runAskPass();

QTextStream standard(stdout);
QTextStream err(stderr);

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QString askpass = QFileInfo(a.applicationFilePath()).absolutePath();

    askpass.append("/aurbrowser-askpass");
    qputenv("SUDO_ASKPASS", askpass.toStdString().c_str());
    a.setApplicationName("AUR Browser");
    a.setApplicationVersion("1.2.1");
    a.setDesktopFileName("AUR-Browser.desktop");
    a.setOrganizationName("B_and_RTechnicalSolutions");

    if(QString(argv[0]).contains("aurbrowser-askpass"))
        return runAskPass();
    else
        return runMainWindow(&a);
}

int runMainWindow(QApplication *app)
{
    MainWindow w;

    w.setWindowTitle("AUR Browser");
    w.show();

    return app->exec();
}

int runAskPass()
{
    bool ok = false;
    QString password = QInputDialog::getText(nullptr, "Sudo password", "Password:", QLineEdit::Password, "", &ok);

    if(ok) {
        standard << password << endl;
        standard.flush();
        return 0;
    }

    err << "Canceled" << endl;
    err.flush();
    return 26;
}
