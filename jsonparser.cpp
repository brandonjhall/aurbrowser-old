/***************************************************************************
 *   Copyright 2018 by Brandon Hall                                        *
 *   brandonjhall@windstream.net                                           *
 *                                                                         *
 *   This file is part of AUR Browser                                      *
 *   AUR Browser is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   AUR Browser is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with AUR Browser. If not, see <http://www.gnu.org/licenses/>.   *
 **************************************************************************/

#define JSON_VERSION 5
#include "jsonparser.h"
#include <QStandardItemModel>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

class JsonParserPrivate {
public:
    JsonParserPrivate(const QJsonDocument doc = QJsonDocument()){document = doc;}
    JsonParserPrivate(const QByteArray array) {
        document = QJsonDocument::fromJson(array);
    }
    QJsonDocument document;
};

const QStringList JsonParser::searchColumns = QStringList() << "URLPath"
                                                            << "Name"
                                                            << "Version"
                                                            << "URL"
                                                            << "NumVotes"
                                                            << "Popularity"
                                                            << "Maintainer"
                                                            << "Description"
                                                            << "PackageBase";

const QStringList JsonParser::multiinfoColumns = QStringList() << "Depends"
                                                               << "MakeDepends"
                                                               << "License"
                                                               << "Keywords"
                                                               << "Provides"
                                                               << "Conflicts";

JsonParser::JsonParser(QObject *parent) : QObject(parent)
{
    priv = new JsonParserPrivate();
    m_model = new QStandardItemModel();
}

JsonParser::JsonParser(const QByteArray data, QObject *parent) : QObject(parent)
{
    priv = new JsonParserPrivate(data);
    m_model = new QStandardItemModel();
}

void JsonParser::setData(const QByteArray data)
{
    priv->document = QJsonDocument::fromJson(data);
}

QString JsonParser::errorString() const
{
    return m_errorString;
}

void JsonParser::processData()
{
    QJsonObject obj = priv->document.object();

    {
        int version = obj.value("version").toInt();
        if(version != JSON_VERSION) {
            m_errorString = QString("Incompatible version. Expected version %1, "
                                    "but got version %2").arg(JSON_VERSION).arg(version);
            emit error();
            return;
        }
    }

    {
        QString type = obj.value("type").toString();

        if(type == "search" || type == "multiinfo")
            m_numResults = obj.value("resultcount").toInt();

        if(type == "search")
            return processSearch(&obj);
        else if(type == "multiinfo")
            return processMultiInfo(&obj);
        else if(type == "error")
            return processError(&obj);

        m_errorString = "Unknown data return type";
        emit error();
    }
}

void JsonParser::processMultiInfo(QJsonObject *obj)
{
    QJsonArray results = obj->value("results").toArray();
    m_details.clear();

    foreach (QJsonValue val, results) {
        if(!val.isObject()) {
            m_errorString = "Unexpected value in JsonArray";
            emit error();
            return;
        }

        QJsonObject result = val.toObject();

        for(int i = 0; i < multiinfoColumns.count(); i++) {
            QStringList list;
            QJsonArray array = result.value(multiinfoColumns.at(i)).toArray();
            list.append(multiinfoColumns.at(i));

            foreach (QJsonValue val, array) {
                list.append(val.toString());
            }
            m_details.append(list);
        }
        emit detailsReady();
        return;
    }
}

void JsonParser::processSearch(QJsonObject *obj)
{
    m_model->clear();
    {
        QStringList labels = searchColumns;

        labels.replace(PKG_VOTE, "Votes");
        m_model->setHorizontalHeaderLabels(labels);
    }

    QJsonArray results = obj->value("results").toArray();

    foreach (QJsonValue val, results) {
        if(!val.isObject()) {
            m_errorString = "Unexpected value in JsonArray";
            emit error();
            return;
        }

        QJsonObject result = val.toObject();
        QList<QStandardItem *> row;
        int columns = searchColumns.count();

        for(int i = 0; i < columns; i++) {
            QJsonValue val1 = result.value(searchColumns.at(i));
            QStandardItem *item = new QStandardItem;
            QVariant value = val1.toVariant();

            item->setData(value, Qt::DisplayRole);
            row.append(item);
        }

        m_model->appendRow(row);
        row.clear();
    }

    emit modelReady();
}

void JsonParser::processError(QJsonObject *obj)
{
    m_errorString = obj->value("error").toString();
    emit error();
}

QList<QStringList> JsonParser::getDetails() const
{
    return m_details;
}

QStringList JsonParser::getMultiinfoColumns()
{
    return multiinfoColumns;
}

QStringList JsonParser::getSearchColumns()
{
    return searchColumns;
}

int JsonParser::numResults() const
{
    return m_numResults;
}

QStandardItemModel *JsonParser::model() const
{
    return m_model;
}
