/***************************************************************************
 *   Copyright 2018 by Brandon Hall                                        *
 *   brandonjhall@windstream.net                                           *
 *                                                                         *
 *   This file is part of AUR Browser                                      *
 *   AUR Browser is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   AUR Browser is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with AUR Browser. If not, see <http://www.gnu.org/licenses/>.   *
 **************************************************************************/
#define MAX_RECENTS 10
#define ABOUT_TEXT "<h3>%1 %2</h3>" \
    "<p>Built with Qt %3 (GCC %4, %5)</p>" \
    "<p>Copyright 2018 Brandon Hall. All rights reserved.</p>" \
    "<p>AUR Browser is distributed in the hope that it will be useful, " \
    "but WITHOUT ANY WARRANTY; without even the implied warranty of " \
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the " \
    "GNU General Public License for more details.</p>"

#include "commandprocessor.h"
#include "networkengine.h"
#include "ui_mainwindow.h"
#include "itemdelegates.h"
#include "jsonparser.h"
#include "mainwindow.h"
#include "bashhighlighter.h"

#include <QStandardItemModel>
#include <QStringListModel>
#include <QDesktopServices>
#include <QTemporaryDir>
#include <QApplication>
#include <QLibraryInfo>
#include <QMessageBox>
#include <QEventLoop>
#include <QScrollBar>
#include <QCompleter>
#include <QSettings>
#include <QSplitter>
#include <QTimer>

class MainWindowPrivate {
  public:
    MainWindowPrivate(){}
    void cdToTempDir() {
        if(tempDir.isValid())
            QDir::setCurrent(tempDir.path());
    }

    QTemporaryDir tempDir;
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow)
{
    QCompleter *completer;
    QSettings appSettings;
    QStringList recents;

    recents = appSettings.value("MainWindow/recents", QStringList()).toStringList();
    completerList = new QStringListModel(recents, this);
    completer = new QCompleter(completerList, this);
    installer = new CommandProcessor(this);
    p = new MainWindowPrivate;

    completer->setCompletionMode(QCompleter::InlineCompletion);
    completer->setCaseSensitivity(Qt::CaseInsensitive);

    ui->setupUi(this);
    ui->splitter->restoreState(appSettings.value("MainWindow/splitter_state").toByteArray());
    ui->fieldCombo->setCurrentIndex(appSettings.value("MainWindow/field_index", 0).toInt());
    ui->sortOrder->setCurrentIndex(appSettings.value("MainWindow/sort_index", 0).toInt());
    ui->sortBy->setCurrentIndex(appSettings.value("MainWindow/sort_by_index", 0).toInt());
    ui->searchBox->setCompleter(completer);
    ui->searchBox->setFocus();
    ui->packageName->clear();
    ui->urlLabel->clear();
    ui->tableView->setItemDelegateForColumn(PKG_POP, new PopularityDelegate);

    restoreGeometry(appSettings.value("MainWindow/geometry").toByteArray());
    restoreState(appSettings.value("MainWindow/state").toByteArray());

    connect(installer, &CommandProcessor::finished, this, [this](const QString &package) {
        statusBar()->showMessage(QString("Finished installing %1.").arg(package), 10000);
        qApp->processEvents();
        ui->scrollArea->verticalScrollBar()->setValue(ui->scrollArea->verticalScrollBar()->maximum());
        p->cdToTempDir();
    });

    connect(installer, &CommandProcessor::outputReady, [this](){
        ui->outputLabel->setText(installer->output());
        QApplication::processEvents();
        ui->scrollArea->verticalScrollBar()->setValue(ui->scrollArea->verticalScrollBar()->maximum());
    });

    QTimer::singleShot(0, this, [this](){initialize();});
}

MainWindow::~MainWindow()
{
    delete ui;
    delete p;
}

void MainWindow::onMoreDetails()
{
    netEngine->getPackageDetails(selectedPackage);
    ui->actionMore_Details->setDisabled(true);
}

void MainWindow::installProgram()
{
    if(downloadableURLs.isEmpty())
        return;

    QEventLoop loop;
    QTimer timer;
    QString url;

    url = downloadableURLs.first();
    ui->outputLabel->clear();

    statusBar()->showMessage("Downloading package from https://aur.archlinux.org");
    netEngine->downloadFile(QUrl("https://aur.archlinux.org" + url));
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    timer.setSingleShot(true);

    connect(netEngine, &NetworkEngine::fileSaved, [this](QString file){currentFile = file;});
    connect(netEngine, &NetworkEngine::fileSaved, &loop, &QEventLoop::quit);
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    timer.start(5000);
    loop.exec();

    QApplication::restoreOverrideCursor();
    statusBar()->clearMessage();

    if(timer.isActive()) {
        installer->makepkg(QString(currentFile).split("/").last().remove(".tar.gz"), currentFile);
    } else {
        QString text = QString("Could not download file from %1").arg("https://aur.archlinux.org" + url);
        QMessageBox mbox(QMessageBox::Warning, "Error", text, QMessageBox::Ok, this);

        mbox.exec();
    }
}

void MainWindow::onSearch()
{
    QStringList recents = completerList->stringList();
    selectedPackage = "";

    if(!recents.contains(ui->searchBox->text(), Qt::CaseInsensitive)) {
        if(recents.count() == MAX_RECENTS)
            recents.removeFirst();

        recents.append(ui->searchBox->text());
        completerList->setStringList(recents);
    }

    ui->actionView_PKGBUILD->setDisabled(true);
    ui->actionView_In_AUR->setDisabled(true);
    ui->actionMore_Details->setDisabled(true);
    ui->actionInstall->setDisabled(true);
    downloadableURLs.clear();
    ui->packageName->clear();
    ui->detailEdit->clear();
    ui->urlLabel->clear();

    netEngine->findPackage(ui->fieldCombo->currentText(), ui->searchBox->text());
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    statusBar()->showMessage("Searching", 5);
}

void MainWindow::about()
{
    QString ver = QVersionNumber(__GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__).toString();
    QString textHtml = QString(ABOUT_TEXT).arg(qApp->applicationDisplayName(), qApp->applicationVersion(),
                                               QLibraryInfo::version().toString(), ver,
                                               (QSysInfo::WordSize) == 64 ? "64 bit" : "32 bit");
    QMessageBox::about(this, qApp->applicationDisplayName(), textHtml);
}

void MainWindow::initialize()
{
    netEngine = new NetworkEngine(this);
    parser = new JsonParser(this);
    QStandardItemModel *model;
    model = parser->model();
    selectedPackage = "";

    ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
    ui->tableView->setSelectionMode(QTableView::SingleSelection);
    ui->tableView->setSelectionBehavior(QTableView::SelectRows);
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
    ui->tableView->setAlternatingRowColors(true);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->setModel(model);

    ui->actionView_PKGBUILD->setDisabled(true);
    ui->actionMore_Details->setDisabled(true);
    ui->actionView_In_AUR->setDisabled(true);
    ui->actionInstall->setDisabled(true);

    netEngine->setTempPath(p->tempDir.path());
    p->cdToTempDir();

    connect(ui->tableView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &MainWindow::onTableClicked);
    connect(netEngine, &NetworkEngine::fileDataReady, this, &MainWindow::onFileDataReady);
    connect(installer, &CommandProcessor::ynPrompt, this, &MainWindow::onYnPrompt);
    connect(parser, &JsonParser::detailsReady, this, &MainWindow::onDetailsReady);
    connect(parser, &JsonParser::error, this, &MainWindow::onError);

    connect(parser, &JsonParser::modelReady, this, [this]() {
        ui->tableView->hideColumn(PKG_PATH);
        ui->tableView->hideColumn(PKG_WEB);
        ui->tableView->hideColumn(PKG_BASE);
        ui->tableView->resizeColumnToContents(PKG_NAME);
        ui->tableView->resizeColumnToContents(PKG_VER);
        ui->tableView->resizeColumnToContents(PKG_VOTE);
        ui->tableView->resizeColumnToContents(PKG_POP);
        ui->tableView->resizeColumnToContents(PKG_MAIN);
        ui->tableView->model()->sort(findSortColumn(), static_cast<Qt::SortOrder>(ui->sortOrder->currentIndex()));
        statusBar()->showMessage(QString("Found %1 records").arg(ui->tableView->model()->rowCount()));
        QApplication::restoreOverrideCursor();
    });

    connect(ui->sortOrder, QOverload<int>::of(&QComboBox::currentIndexChanged), this,
            [model, this](int index) {
            model->sort(findSortColumn(), static_cast<Qt::SortOrder>(index));
    });

    connect(ui->sortBy, QOverload<const QString&>::of(&QComboBox::currentIndexChanged), this,
            [model, this](const QString &text) {
            model->sort(findSortColumn(text), static_cast<Qt::SortOrder>(ui->sortOrder->currentIndex()));
    });

    connect(netEngine, &NetworkEngine::dataReady, this, [this]() {
        parser->setData(netEngine->getData());
        parser->processData();
    });

    connect(ui->tableView, &QTableView::customContextMenuRequested, ui->tableView, [this](QPoint pos) {
        if(!ui->tableView->currentIndex().isValid())
            return;

        QMenu ctxMenu(this);

        ctxMenu.addActions(QList<QAction *>() << ui->actionMore_Details << ui->actionInstall
                           << ui->actionView_PKGBUILD << ui->actionView_In_AUR);
        ctxMenu.exec(ui->tableView->mapToGlobal(pos));
    });
}

void MainWindow::closeEvent(QCloseEvent *)
{
    saveSettings();
}

void MainWindow::viewCurrentPKGBUILD()
{
    QString base = "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=";
    netEngine->downloadFileToMemory(QUrl(base + selectedPkgBase));
}

void MainWindow::viewInAUR()
{
    QString base = "https://aur.archlinux.org/packages/";
    QDesktopServices::openUrl(QUrl(base + selectedPackage));
}

void MainWindow::onFileDataReady()
{
    QDialog *dialog = new QDialog(this);
    QPlainTextEdit *viewer = new QPlainTextEdit(netEngine->getFileData(), dialog);
    QSyntaxHighlighter *high = new BashHighlighter(viewer->document());
    QVBoxLayout *layout = new QVBoxLayout(dialog);

    Q_UNUSED(high)

    dialog->restoreGeometry(dialogState);
    viewer->setWordWrapMode(QTextOption::NoWrap);
    dialog->setLayout(layout);
    layout->addWidget(viewer);
    dialog->setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);
    dialog->setWindowModality(Qt::ApplicationModal);
    dialog->show();
    viewer->document()->adjustSize();
    QApplication::processEvents();
    viewer->resize(viewer->document()->size().width() + 5, viewer->height());
    dialog->resize(viewer->document()->size().width() + 50, dialog->height());
    dialog->hide();
    QApplication::processEvents();
    dialog->exec();
    dialogState = dialog->saveGeometry();
    dialog->deleteLater();
}

int MainWindow::findSortColumn(QString text)
{
    if(text.isEmpty())
        text = ui->sortBy->currentText();

    if(text == "Name")
        return PKG_NAME;

    if(text == "Votes")
        return PKG_VOTE;

    if(text == "Popularity")
        return PKG_POP;

    if(text == "Maintainer")
        return PKG_MAIN;

    return 0;
}

void MainWindow::saveSettings()
{
    QSettings appSettings;

    appSettings.beginGroup("MainWindow");

    appSettings.setValue("field_index", ui->fieldCombo->currentIndex());
    appSettings.setValue("sort_index", ui->sortOrder->currentIndex());
    appSettings.setValue("sort_by_index", ui->sortBy->currentIndex());
    appSettings.setValue("splitter_state", ui->splitter->saveState());
    appSettings.setValue("recents", completerList->stringList());
    appSettings.setValue("geometry", saveGeometry());
    appSettings.setValue("state", saveState());
}

void MainWindow::onYnPrompt(const QString &prompt, const QChar &defaultChoice)
{
    ui->scrollArea->verticalScrollBar()->setValue(ui->scrollArea->verticalScrollBar()->maximum());
    QApplication::processEvents();

    QMessageBox msgBox(QMessageBox::NoIcon, "Yes/No", prompt, QMessageBox::Yes, this);
    msgBox.addButton(QMessageBox::No);

    if(defaultChoice == 'Y')
        msgBox.setDefaultButton(QMessageBox::Yes);
    else
        msgBox.setDefaultButton(QMessageBox::No);

    if(msgBox.exec() == QMessageBox::Yes)
        installer->accept();
    else
        installer->decline();
}

void MainWindow::onTableClicked(const QItemSelection &selection)
{
    if(selection.isEmpty())
        return;

    QAbstractItemModel *model = ui->tableView->model();
    QModelIndexList indexes = selection.indexes();

    downloadableURLs.clear();
    downloadableURLs.append(model->data(indexes.first()).toString());
    QString description = model->data(indexes.at(PKG_DESC)).toString();
    QString packageName = model->data(indexes.at(PKG_NAME)).toString();
    QString website = model->data(indexes.at(PKG_WEB)).toString();
    selectedPkgBase = model->data(indexes.at(PKG_BASE)).toString();

    ui->detailEdit->clear();
    ui->urlLabel->setText(QString("<a href=\"%1\">%1</a>").arg(website));
    ui->detailEdit->appendPlainText(description);
    ui->packageName->setText(packageName);
    selectedPackage = packageName;

    ui->actionView_PKGBUILD->setEnabled(true);
    ui->actionMore_Details->setEnabled(true);
    ui->actionView_In_AUR->setEnabled(true);
    ui->actionInstall->setEnabled(true);
}

void MainWindow::onDetailsReady()
{
    ui->detailEdit->appendPlainText(" ");
    foreach (QStringList list, parser->getDetails()) {
        ui->detailEdit->appendPlainText(list.takeFirst() + ":");

        foreach(QString text, list)
            ui->detailEdit->appendPlainText("    " + text);

        ui->detailEdit->appendPlainText(" ");
    }
}

void MainWindow::onError()
{
    QApplication::restoreOverrideCursor();
    QMessageBox mbox(this);

    statusBar()->clearMessage();
    parser->model()->clear();

    QApplication::processEvents();

    mbox.setText(parser->errorString());
    mbox.setWindowTitle("Error");
    mbox.exec();
}
