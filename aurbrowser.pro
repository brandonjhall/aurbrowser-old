#-------------------------------------------------
#
# Project created by QtCreator 2018-01-27T13:03:29
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = aurbrowser
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    jsonparser.cpp \
    commandprocessor.cpp \
    networkengine.cpp \
    bashhighlighter.cpp

HEADERS += \
        mainwindow.h \
    jsonparser.h \
    commandprocessor.h \
    itemdelegates.h \
    networkengine.h \
    bashhighlighter.h

FORMS += \
        mainwindow.ui

RESOURCES += \
    icons.qrc

DISTFILES += \
    README.md \
    COPYING \
    AUR-Browser.desktop \
    aurbrowser.png \
    TODO.md

isEmpty(INSTALL_PREFIX) {
    INSTALL_PREFIX = /usr/local
}

desktopfile.path = $$INSTALL_PREFIX/share/applications
desktopfile.files = AUR-Browser.desktop
icon.path = $$INSTALL_PREFIX/share/icons/hicolor/48x48/apps
icon.files = aurbrowser.png
target.path = $$INSTALL_PREFIX/bin
link.files = aurbrowser-askpass
link.path = $$INSTALL_PREFIX/bin
link.extra = ln -srf $$INSTALL_PREFIX/bin/aurbrowser $$INSTALL_PREFIX/bin/aurbrowser-askpass
INSTALLS += target
INSTALLS += link
INSTALLS += desktopfile
INSTALLS += icon
