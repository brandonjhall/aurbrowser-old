#ifndef NETWORKENGINE_H
#define NETWORKENGINE_H
#include <QNetworkAccessManager>

class NetworkEngine : public QNetworkAccessManager
{
    Q_OBJECT
public:
    explicit NetworkEngine(QObject *parent = nullptr);
    void findPackage(const QString field, const QString searchText);
    void getPackageDetails(const QString pkgName);
    void downloadFileToMemory(QUrl url);
    void downloadFile(QUrl url);

    void setTempPath(const QString &tempPath);
    QString getFileData() const;
    QByteArray getData();

signals:
    void fileSaved(QString fileName);
    void fileDataReady();
    void dataReady();

private:
    void sendRPCRequest(QUrl url);
    void error(QNetworkReply *reply);

    QString m_tempPath;
    QString m_fileData;
    QByteArray m_data;
};
#endif // NETWORKENGINE_H
