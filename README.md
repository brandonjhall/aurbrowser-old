# AUR Browser

A Qt application that allows an Arch Linux user to browse the AUR.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
qt5-base 5.10.0-3
gcc-libs 7.2.1+20180116-1
glibc 2.26-11
```

### Installing

To install AUR Browser, run qmake and make install.
```
qmake
make install
```

The default install path is /usr/local/bin; to change this, set the INSTALL_PREFIX with qmake.
```
qmake INSTALL_PREFIX=/usr
make install
```
## Built With

* [Qt 5.10](https://doc.qt.io/qt-5.10/)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/brandonjhall/aurbrowser/tags).

## Authors

* **Brandon Hall**

## License

This project is licensed under the GNU General Public License - see the [COPYING](COPYING) file for details

