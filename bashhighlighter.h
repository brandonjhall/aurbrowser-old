#ifndef BASHHIGHLIGHTER_H
#define BASHHIGHLIGHTER_H
#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QRegularExpression>

class BashHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    BashHighlighter(QObject *parent);
    BashHighlighter(QTextDocument *parent);

protected:
    void highlightBlock(const QString &text);

private:
    void setupRules();

    struct HighlightingRule
    {
        QRegularExpression pattern;
        QTextCharFormat format;
    };
    QVector<HighlightingRule> highlightingRules;

    QRegularExpression commentStartExpression;

    QTextCharFormat keywordFormat;
    QTextCharFormat commandFormat;
    QTextCharFormat singleLineCommentFormat;
    QTextCharFormat singleQuoteFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat functionFormat;
};

#endif // BASHHIGHLIGHTER_H
