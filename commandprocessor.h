/***************************************************************************
 *   Copyright 2018 by Brandon Hall                                        *
 *   brandonjhall@windstream.net                                           *
 *                                                                         *
 *   This file is part of AUR Browser                                      *
 *   AUR Browser is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   AUR Browser is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with AUR Browser. If not, see <http://www.gnu.org/licenses/>.   *
 **************************************************************************/

#ifndef COMMANDPROCESSOR_H
#define COMMANDPROCESSOR_H

#include <QObject>

class QProcess;

class CommandProcessor : public QObject
{
    Q_OBJECT
public:
    explicit CommandProcessor(QObject *parent = nullptr);
    void makepkg(const QString &package,const QString &archive);
    QString output() const;
    void decline();
    void accept();

signals:
    void outputReady();
    void finished(const QString &packageName);
    void ynPrompt(const QString &prompt, const QChar &defaultChoice);
    void ynDetailPrompt(const QString &prompt, const QChar &defaultChoice, const QString &details);

public slots:
    void buildPackage();
    void installPackage();
    void extractTar(QString file);

private:
    QProcess *setupProcess();
    void processOutput();
    void queNext();

    QList<QPair<QString,QString>> que;
    QProcess *currentProcess;
    QString packageDir = "";
    QString currentPackage;
    bool isRunning = false;
    char *nextFunction;
    QString m_output;
};

#endif // COMMANDPROCESSOR_H
