#include "bashhighlighter.h"

BashHighlighter::BashHighlighter(QObject *parent) : QSyntaxHighlighter(parent)
{
    setupRules();
}

BashHighlighter::BashHighlighter(QTextDocument *parent) : QSyntaxHighlighter(parent)
{
    setupRules();
}

void BashHighlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules) {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(text);
        while (matchIterator.hasNext()) {
            QRegularExpressionMatch match = matchIterator.next();
            setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
    }
    setCurrentBlockState(0);
}

void BashHighlighter::setupRules()
{
    HighlightingRule rule;

    keywordFormat.setForeground(Qt::darkRed);
    keywordFormat.setFontWeight(QFont::Bold);
    QStringList keywordPatterns;

    keywordPatterns << "\\bcoproc\\b" << "\\belif\\b" << "\\bfi\\b" << "\\bif\\b" << "\\bthen\\b"
                    << "\\bwhile\\b" << "\\bdo\\b" << "\\belse\\b" << "\\bfor\\b" << "\\bin\\b"
                    << "\\btime\\b" << "\\bcase\\b" << "\\bdone\\b" << "\\besac\\b" << "\\bfunction\\b"
                    << "\\bselect\\b" << "\\buntil\\b";

    foreach (const QString &pattern, keywordPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }

    commandFormat.setFontWeight(QFont::Bold);
    QStringList commandPatterns;
    commandPatterns << "\\bcd\\b" << "\\bmake\\b" << "\\bsed\\b" << "\\binstall\\b" << "\\brm\\b"
                    << "\\brmdir\\b" << "\\bpython\\b" << "\\bqmake\\b" << "\\bcp\\b" << "\\bmv\\b"
                    << "\\bmkdir\\b" << "\\bsh\\b" << "\\bbash\\b" << "\\bpushd\\b" << "\\bpopd\\b";

    foreach (const QString &pattern, commandPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = commandFormat;
        highlightingRules.append(rule);
    }

    quotationFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegularExpression("\".*\"");
    rule.format = quotationFormat;
    highlightingRules.append(rule);

    singleQuoteFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegularExpression("'.*'");
    rule.format = singleQuoteFormat;
    highlightingRules.append(rule);

    functionFormat.setFontItalic(true);
    functionFormat.setForeground(Qt::blue);
    rule.pattern = QRegularExpression("\\b[A-Za-z0-9_-]+(?=\\()");
    rule.format = functionFormat;
    highlightingRules.append(rule);

    singleLineCommentFormat.setForeground(Qt::darkGray);
    rule.pattern = QRegularExpression("#[^\n]*");
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);
}
